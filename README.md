# ics-ans-role-phoebus

Ansible role to install phoebus.

## Role Variables

```yaml
---
phoebus_installation_dir: /opt
phoebus_version: 4.6.3
phoebus_repository: "{{ (phoebus_version[-9:] == '-SNAPSHOT') | ternary('libs-snapshot', 'libs-release') }}"
phoebus_archive: "http://artifactory.esss.lu.se/artifactory/{{ phoebus_repository }}/se/europeanspallationsource/ics/ess-cs-studio-phoebus/{{ phoebus_version }}/ess-cs-studio-phoebus-{{ phoebus_version }}-linux.tar.gz"
phoebus_settings_path: "{{ phoebus_home }}/phoebus.ini"
phoebus_logback_config: "{{ phoebus_home }}/logback.xml"
phoebus_settings: {}
phoebus_options: "-logging {{ phoebus_home }}/logging.properties"
phoebus_ess_templates_version: 1.0.4
phoebus_ess_symbols_version: 1.0.5
phoebus_ess_opis_version: master
phoebus_opis_repos: []
phoebus_git_folders_base: /usr/local/share/cs-studio
phoebus_opis_path: "{{ phoebus_git_folders_base }}/opis"
phoebus_ess_symbols_path: "{{ phoebus_git_folders_base }}/ess-symbols"
phoebus_ess_templates_path: "{{ phoebus_git_folders_base }}/ess-templates"
# Set to [] to not create a python environment
phoebus_python_packages:
  - python=3.7
  - py4j
  - connect2j
  - numpy=1.17
  - scipy=1.3
  - pandas=0.25
```

The `-server 4918` option can be added by overriding the `phoebus_options` variable.

The `phoebus_opis_repos` variable is a list of dictionaries with the following keys:

- url: URL to the git repository (mandatory)
- version: version of the repo [default: HEAD]
- dest: directory name where to clone the repo [default: name of the git repo]

Note that `dest` is the relative path under `phoebus_opis_path`.
The following variables:

```yaml
phoebus_opis_path: /data/opis-pbi
phoebus_opis_repos:
  - url: https://gitlab.esss.lu.se/beam-diagnostics/opis-pbi/opis-pbi-engineer.git
    dest: "10-Engineer/10-ACC/99-PBI"
  - url: https://gitlab.esss.lu.se/beam-diagnostics/opis-pbi/opis-pbi-operator.git
    dest: "30-Operator/10-ACC/99-PBI"
```

Would create:

```bash
/data/
`-- opis-pbi
    |-- 10-Engineer
    |   `-- 10-ACC
    |       `-- 99-PBI
    `-- 30-Operator
        `-- 10-ACC
            `-- 99-PBI
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-phoebus
```

## License

BSD 2-clause
