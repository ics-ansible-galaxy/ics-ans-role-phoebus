import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_default(host):
    cmd = host.run("/usr/local/bin/phoebus_current -help")
    assert cmd.rc == 0
    assert (
        " _______           _______  _______  ______            _______ \n"
        + "(  ____ )|\\     /|(  ___  )(  ____ \\(  ___ \\ |\\     /|(  ____ \\\n"
        + "| (    )|| )   ( || (   ) || (    \\/| (   ) )| )   ( || (    \\/\n"
        + "| (____)|| (___) || |   | || (__    | (__/ / | |   | || (_____ \n"
        + "|  _____)|  ___  || |   | ||  __)   |  __ (  | |   | |(_____  )\n"
        + "| (      | (   ) || |   | || (      | (  \\ \\ | |   | |      ) |\n"
        + "| )      | )   ( || (___) || (____/\\| )___) )| (___) |/\\____) |\n"
        + "|/       |/     \\|(_______)(_______/|/ \\___/ (_______)\\_______)"
        in cmd.stdout
    )


def test_settings(host):
    settings = host.file("/opt/ess-cs-studio-phoebus-5.0.030/phoebus.ini")
    assert settings.exists
    assert settings.is_file
    assert settings.contains("org.phoebus.pv.ca/addr_list=127.0.0.1")


def test_opis_repositories(host):
    if (
        host.ansible.get_variables()["inventory_hostname"]
        != "ics-ans-role-phoebus-onerepo"
    ):
        for filename in (
            "/usr/local/share/cs-studio/ess-opis/README.md",
            "/data/opis-pbi/opis-pbi-systemexpert/README.md",
            "/data/opis-pbi/10-Engineer/10-ACC/99-PBI/README.md",
            "/data/opis-pbi/30-Operator/10-ACC/99-PBI/README.md",
            "/data/opis-pbi/20-SystemExpert/10-ACC/999-Cryo/README.md",
        ):
            assert host.file(filename).exists
        cmd = host.run("cd /data/opis-pbi/opis-pbi-systemexpert;git rev-parse HEAD")
        assert cmd.stdout.strip() == "1d34327ea88a12fec5e64c38839e038361e0a660"
    else:
        for filename in (
            "/data/opis-pbi/10-Engineer/10-ACC/99-PBI/README.md",
        ):
            assert host.file(filename).exists
        cmd = host.run("cd /data/opis-pbi/10-Engineer/10-ACC/99-PBI;git rev-parse HEAD")
        assert cmd.stdout.strip() == "fd19d4765d8cee0480f2bfa86efb72e1051fad69"
        for filename in (
            "/data/opis-pbi/opis-pbi-systemexpert/README.md",
            "/data/opis-pbi/30-Operator/10-ACC/99-PBI/README.md",
            "/data/opis-pbi/20-SystemExpert/10-ACC/999-Cryo/README.md",
        ):
            assert host.file(filename).exists is False


def test_opis_repositories_are_deleted(host):
    for filename in (
        "/data/opis-pbi/20-SystemExpert/10-ACC/999-TS2/99-RF/README.md",
        "/data/opis-pbi/10-Engineer/10-ACC/999-TS2/99-RF/README.md",
        "/data/opis-pbi/30-Operator/10-ACC/000-Ops/README.md",
    ):
        assert host.file(filename).exists is False


def test_opi_navigators(host):
    for filename in (
        "/usr/local/share/cs-studio/opi-navigators/mcr-operations/main/README.md",
    ):
        assert host.file(filename).exists
